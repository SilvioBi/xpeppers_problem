# README #
This repository is the resolution of a programming problem by XPeppers.
### set up ###
If you have Python than just run the script with **python <path_to_repo>/homework/homework.py** . Otherwise get Python [here](https://www.python.org/). It works with Python 2 and should work (not tested) with Python 3
### further info ###
* Silvio Biasiol 
* [XPeppers team](http://www.xpeppers.com/)