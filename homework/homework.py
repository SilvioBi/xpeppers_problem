#!/usr/bin/env python
from decimal import Decimal, ROUND_UP, ROUND_DOWN
'''This script is a solution to the problem that can be found in this repository, named problem.txt. 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details. '''

__author__ = "Silvio Biasiol"
__copyright__ = "Copyright 2017, Silvio Biasiol"
__license__ = "GPL"
__version__ = "1.0.0"
__email__ = "biasiol.silvio@gmail.com"
__status__ = "Production"

# A string representing the input file, it has been hardcoded for simplicity purpouses. 
# I've assumed for simplicity in the code that the input are always in the same
# format : 
# string composed by sequence of '<quantity> <name of product and eventually 'imported' substring> 'at' <price>'
input_text = {
    '1 book at 12.49 1 music CD at 14.99 1 chocolate bar at 0.85',
    '1 imported box of chocolates at 10.00 1 imported bottle of perfume at 47.50',
    '1 imported bottle of perfume at 27.99 1 bottle of perfume at 18.99 1 packet of headache pills at 9.75 1 box of imported chocolates at 11.25'
}

# A dictionary storing all the possible products names in the format <name>: <category>
dictionary = {
    'book': 'book',
    'music CD' : 'general',
    'chocolate bar' : 'food',
    'box of chocolates' : 'food',
    'bottle of perfume' : 'general',
    'packet of headache pills' : 'drug'
}

def round_up(num):
    """Rounds up a number to the nearest 0.05
        Attributes:
            num: A decimal to be rounded up. """
    # first let's compute the difference between num and num rounded up to the next decimal
    # (ex. 8.23 -> 8.30) then let's compare it to 0.05 
    comp_to_rounded_up = (Decimal('0.05').compare(
                          num.quantize(Decimal('.1'), rounding=ROUND_UP) 
                        - num.quantize(Decimal('.01'), rounding=ROUND_UP)))
    # if is greater or equal to 0.05 than the number is 
    # in the format -.-<1-5> so return it as -.-5
    if comp_to_rounded_up == -1 or comp_to_rounded_up == 0:
        return (num.quantize(Decimal('.1'), rounding=ROUND_DOWN)
                + Decimal(0.05)).quantize(Decimal('.01'))
    # if 0.05 is greater return the next decimal (ex. 12.26 -> 12.30)
    return num.quantize(Decimal('.1'), rounding=ROUND_UP).quantize(Decimal('.01'))

class Product:
    """This class represent a product.

        Represents a generic product,
        used in the Receipt class.

    Attributes:
        imported: A boolean indicating wether the product is imported or not.
        name: A string representing the product name.
        price: A decimal representing the product price.
        category: A string stating the product category, it can be 'food' | 'general' | 'drug'. """
    def __init__(self, tokens):
        """Inits Product class taking as input a list of strings. 
        Attributes:
            tokens: A list of strings, the last string in the list must be a number 
                    representing the price, all the other tokens are used to compose 
                    the name and state if the product is imported or not. """
        # check if 'imported' substring is in the string obtained by unifying the tokens
        # except the last two tokens because they are 'at' <price>
        self.imported = True if 'imported' in ' '.join(tokens[1:-2]) else False
        # once checked if is imported let's join all the tokens (except the last two)
        # and remove the 'imported' substring if present
        self.name = ' '.join(tokens[1:-2]).replace('imported ', '')
        self.price = Decimal(tokens[-1])
        # check if the product's category is in the dictionary. It must be present in the 
        # dictionary to avoid incorrect behaviour so I've decided to use an assertion
        self.category = dictionary.get(self.name)
        assert self.category

    def __str__(self):
        """Overrides the method __str__ returning a string representation
           of a Product instance when the method str() is invoked"""
        if self.imported:
            return  'imported ' + self.name + ' ' + str(self.price)
        return  self.name + ' ' + str(self.price)

class Receipt:
    """This class represent a receipt.

        Represents a generic receipt, used to
        compute taxes and print a list of products.
        All variables in the Receipt class are marked as 
        protected so they should not be used outside the class.

    Attributes:
        products: A list of Products used to store the products in the receipt.
        total: A decimal representing the total amount of the receipt.
        taxes: A decimal representing the total amount of taxes applied. """
    def __init__(self, text):
        """Inits Receipt class taking as input a string.

        Attributes:
            text: A string representing a list of product each in the format:
                  <quantity> <product name and (eventually) substring 'imported'> 'at' <price>.
                  For example:
                  '1 box of imported chocolates at 11.25'. """
        self._products = []
        self._total = 0
        self._taxes = 0

        token_buffer = []
        # the input file is splitted in a list of strings every time
        # a space is encountered
        for word in text.split():
            # if the string is an integer than it is the quantity
            if word.isdigit():
                # this statement below is used only the first time the loop starts
                # because it encounters the first quantity but nothing is yet
                # stored in the token_buffer
                if token_buffer:
                    quantity = Decimal(token_buffer[0])
                    self.addProduct(quantity, Product(token_buffer))
                # once the product and its quantity is stored the token_buffer
                # is emptyed to use it for the next product
                del token_buffer[:]
            # let's add a word to the token buffer
            token_buffer.append(word)
        # once the loop is finished either we have the last product to
        # store in the receipt or the were no product at all so let's check it and 
        # eventually add the product
        if token_buffer:
            quantity = Decimal(token_buffer[0])
            self.addProduct(quantity, Product(token_buffer))
        
    def __str__(self):
        """Returns a string representation
           of a Receipt instance when the method print() is invoked,
           listing all the products, the taxes and the total. """
        str_repr = ''
        for quantity, product in self._products: 
            str_repr += str(quantity) + ' ' + str(product) + ' '
        str_repr += 'Sales Taxes: '+ str(self._taxes) + ' ' + 'Total: ' + str(self._total) + '\n'
        return str_repr

    def addProduct(self, quantity, product):
        """Add a product to the current Receipt instance.

        Attributes:
            quantity: A decimal representing how many products to add
            product: A Product to be added in the receipt. """
        self._products.append((quantity, product))
        # the tax is 10% if is not a drug, a book or food
        tax_perc = Decimal('0.1') if product.category not in ['drug', 'book', 'food'] else Decimal('0')
        # there is an additional tax of 5% on all imported products
        if product.imported: tax_perc += Decimal('0.05')
        tax_applied = round_up(product.price * tax_perc)
        self._taxes += round_up(quantity * tax_applied)
        product.price += tax_applied
        self._total += quantity * product.price

'''Main loop. The if condition is to run the main loop only if this script is directly executed
and not imported. '''
if __name__ == '__main__':
    receipts = [ Receipt(line) for line in input_text ]
    for receipt in receipts: print receipt
